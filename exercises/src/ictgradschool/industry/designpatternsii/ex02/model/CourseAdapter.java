package ictgradschool.industry.designpatternsii.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

	private Course course;


	public CourseAdapter(Course course) {
		this.course = course;
		course.addCourseListener(this);
	}

	@Override
	public int getRowCount() {
		return course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	final int StudentId = 0;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex){
			case StudentId:
				return course.getResultAt(rowIndex)._studentID;
			case 1:
				return course.getResultAt(rowIndex)._studentSurname;
			case 2:
				return course.getResultAt(rowIndex)._studentForename;
			case 3:
				return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}
		return null;
	}

	@Override
	public String getColumnName(int column) {
		switch (column){
			case StudentId:
				return "Student ID";
			case 1:
				return "Surname";
			case 2:
				return "Forename";
			case 3:
				return "Exam";
			case 4:
				return "Text";
			case 5:
				return "Assignment";
			case 6:
				return "Overall";
		}
		return null;
	}

	@Override
	public void courseHasChanged(Course course) {
		//把新的course内容传入panel
		fireTableDataChanged();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}