package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {
	
	/**********************************************************************
	 * YOUR CODE HERE
	 */
	private StatisticsPanel panel;
	private Course course;
	public StatisticsPanelAdapter(StatisticsPanel panel, Course course) {
		this.panel = panel;
		this.course = course;
		this.course.addCourseListener(this);
	}

	@Override
	public void courseHasChanged(Course course) {
		this.panel.repaint();
	}
}
