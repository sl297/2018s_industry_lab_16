package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {

	private  DistributionPanel panel;
	private  Course course;

	public DistributionPanelAdapter(DistributionPanel panel, Course course) {
		this.panel = panel;
		this.course = course;
		// this course is listen to this adapter
		this.course.addCourseListener(this);
		// this listener is try to listen to the course, if that has any change.
		// if so, then  it will call the coursehaschanged function.
	}

	// try to moniter the course
	@Override
	public void courseHasChanged(Course course) {
		this.panel.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
